package mx.santander.fiduciario.instruccionconsulta.util;

/**
 * @author David Gonzalez - (Arquetipo creado por Santander Tecnologia Mexico)
 * 
 * Clase de constantes para el microservicio
 */
public final class InstruccionConsultaConstantes {
	
	public static final String ERROR = "Error";

	public static final String WARNING = "Warning";
	
	//Agregar nuevas constantes aqui
	
	
	private InstruccionConsultaConstantes() {} // previene instanciacion
	
}